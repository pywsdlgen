#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author:  Enrico Tröger
# License: LGPL
#

from optparse import OptionParser
import os
import sys

from pywsdlgen.parser_re import ParserRe
from pywsdlgen.parser_intro import ParserIntrospection



description = """PyWsdlGen is a little tool to generate a Web Services Description
Language (WSDL) file from Python source files.
It parses a given Python source file and reads all public methods
defined in this file to generate a WSDL file to be used for example
for SOAP services implemented in Python.
"""


#----------------------------------------------------------------------
def setup_options(parser):
	"""
	Set up options and defaults

	@param parser (optparse.OptionParser())
	"""
	parser.add_option(
		'-n', '--name',
		dest='name',
		default='',
		help=u'the name of the new SOAP service')
	parser.add_option(
		'-t', '--template',
		dest='template_file',
		default='pywsdlgen/WSDL.template',
		help=u'the filename of the template file to be used')
	parser.add_option(
		'-u', '--url',
		dest='url',
		default='',
		help=u'the destination URL of the new SOAP service')
	parser.add_option(
		'-o', '--output',
		dest='output_file',
		default='WSDL',
		help=u'the filename to write the generated SOAP service WSDL')

#----------------------------------------------------------------------
def main():
	option_parser = OptionParser(usage='USAGE: %s [options] WSDL' % sys.argv[0], description=description)
	setup_options(option_parser)
	arg_options, args = option_parser.parse_args()

	if len(args) != 1:
		print >> sys.stderr, 'Expecting a file as argument (.py).\nRun %s --help for details.' % sys.argv[0]
		sys.exit(os.EX_USAGE)

	if not arg_options.url:
		print >> sys.stderr, 'You need to specify a target URL.\nRun %s --help for details.' % sys.argv[0]
		sys.exit(os.EX_USAGE)

	parser = ParserIntrospection(name=arg_options.name, url=arg_options.url, template_file=arg_options.template_file)
	parser.process_file(args[0])
	parser.write_wsdl(arg_options.output_file)

if __name__ == '__main__':
	main()

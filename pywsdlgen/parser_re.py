#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author:  Enrico Tröger
# License: LGPL



import re
import sys
from parser import Parser

STATE_NORMAL = 0
STATE_IN_DOC = 1


class ParserRe(Parser):
	"""
	Regular expression based parser implementation
	(partly based on the script at http://svn.python.org/view/*checkout*/python/trunk/Tools/scripts/ptags.py)
	"""

	#----------------------------------------------------------------------
	def __init__(self, **kw):
		super(ParserRe, self).__init__(**kw)

		self.re_function = re.compile('^[ \t]*def[ \t]+([a-zA-Z0-9_]+)[ \t]*(\(.*\))[:]')
		self.re_clean_args = re.compile('(\(|\)|[ \t])')
		self.re_doc_string = re.compile('^[ \t]*@(param|return)[ \t]+(.*)[ \t]+(.*).*')

	#----------------------------------------------------------------------
	def _add_tag(self, tagname, args):
		"""
		Verify the found tag name and if it is valid, add it to the list

		@param tag (str)
		@param args (str)
		"""
		args_dict = { 'return': '' }
		if args:
			tmp = args.split(',')
			for arg in tmp:
				arg = self.re_clean_args.sub('', arg)
				# ignore the self argument
				if arg != 'self':
					# for now, we don't know yet the type of the argument, so don't set it
					args_dict[arg] = ''
		self.tags[tagname] = args_dict

	#----------------------------------------------------------------------
	def _parse_doc_string(self, input):
		"""
		Read the file specified by filename and look for class and function definitions

		@param input (str)
		@return arg_name, arg_type (tuple)
		"""
		arg_type = ''
		arg_name = ''
		if input:
			m = self.re_doc_string.match(input)
			if m:
				f1, f2, f3 = m.groups()
				if f2 and f2[0] == '(':
					arg_name = f3
					arg_type = f2
				else:
					arg_name = f2
					arg_type = f3

				arg_type = self.re_clean_args.sub('', arg_type)
				if f1 == 'return':
					arg_name = 'return'

		return (arg_name, arg_type)

	#----------------------------------------------------------------------
	def process_file(self, filename):
		"""
		Read the file specified by filename and look for class and function definitions

		@param filename (str)
		"""
		try:
			fp = open(filename, 'r')
		except:
			print >> sys.stderr, 'Cannot open %s' % (filename)
			return
		last_tag = ''
		state = STATE_NORMAL
		for line in fp:
			line = line.strip()
			if line == '"""' or line == '\'\'\'':
				if state == STATE_IN_DOC:
					state = STATE_NORMAL
					continue
				else:
					state = STATE_IN_DOC
			elif state == STATE_IN_DOC:
				if line and line[0] == '@' and last_tag in self.tags:
					arg_name, arg_type = self._parse_doc_string(line)
					# if we found a type in the doc string and this argument is also in
					# the list of arguments we parsed before, then add the type
					if arg_type and arg_name in self.tags[last_tag]:
						self.tags[last_tag][arg_name] = arg_type
			else:
				m = self.re_function.match(line)
				if m:
					name, args = m.groups()
					# only public functions
					if name and name[0] != '_':
						args = args.strip()
						last_tag = name
						self._add_tag(name, args)
					state = STATE_NORMAL



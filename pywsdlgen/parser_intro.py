#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author:  Enrico Tröger
# License: LGPL



import imp
import inspect
import re
import sys
from parser import Parser


class ParserIntrospection(Parser):
	"""
	Parser based on introspection code
	"""

	#----------------------------------------------------------------------
	def __init__(self, **kw):
		super(ParserIntrospection, self).__init__(**kw)

		self.re_doc_string = re.compile('^[ \t]*@(param|return)[ \t]+(.*)[ \t]+(.*).*')
		self.re_clean_args = re.compile('(\(|\)|[ \t])')

	#----------------------------------------------------------------------
	def _add_tag(self, tagname, args):
		"""
		Verify the found tag name and if it is valid, add it to the list

		@param tagname (str)
		@param args (list)
		"""
		# add two lists, one for the arguments and the other one for the argument types
		# a dcitionary would be much better but then we will loose the argument order
		# TODO use a lift of tuples
		arg_types = []
		args.remove('self')
		# TODO find a more elegant way to create an empty list of X elements
		args_len = len(args)
		for x in xrange(0, args_len + 1):
			arg_types.append('')
		args.append('return')
		self.tags[tagname] = [ args, arg_types ]

	#----------------------------------------------------------------------
	def _parse_doc_string(self, method_name, input):
		"""
		Parse the given doc string for arguments and argument types

		@param method_name (str)
		@param input (str)
		"""
		if input:
			lines = input.splitlines()
			for line in lines:
				m = self.re_doc_string.match(line)
				if m:
					f1, f2, f3 = m.groups()
					if f2 and f2[0] == '(':
						arg_name = f3
						arg_type = f2
					else:
						arg_name = f2
						arg_type = f3

					arg_type = self.re_clean_args.sub('', arg_type)
					if f1 == 'return':
						arg_name = 'return'

					if arg_type:
						try:
							idx = self.tags[method_name][0].index(arg_name)
							self.tags[method_name][1][idx] = arg_type
						except ValueError:
							pass

	#----------------------------------------------------------------------
	def process_file(self, filename):
		"""
		Read the file specified by filename and look for class and function definitions

		@param filename (str)
		"""
		module = imp.load_source('pywsdlgen', filename)
		symbols = inspect.getmembers(module, callable)
		for name, obj in symbols:
			if inspect.isclass(obj):
				self.class_name = obj.__name__
				methods = inspect.getmembers(obj, inspect.ismethod)
				for m_name, m_obj in methods:
					# skip non-public tags and non-methods
					if m_name.startswith('_') or not inspect.ismethod(m_obj):
						continue

					try:
						args = inspect.getargspec(m_obj).args
					except:
						args = ''
					self._add_tag(m_name, args)
					self._parse_doc_string(m_name, m_obj.__doc__)











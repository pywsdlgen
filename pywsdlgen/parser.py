#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Author:  Enrico Tröger
# License: LGPL


import sys
import re
from string import Template
from xml.parsers.expat import ExpatError
from xml.dom.minidom import parseString


# mapping between Python types and xsd/soapenc types
# keys: Python types, values: SOAP types
type_map = {
	'bool': 'boolean',
	'int': 'int',
	'long': 'long',
	'decimal': 'float',
	'float': 'float',
	'double': 'double',
	'datetime': 'dateTime',
	'date': 'date',
	'time': 'time',
	'str': 'string'
}

class Parser(object):

	#----------------------------------------------------------------------
	def __init__(self, **kw):
		self.tags = {}
		self.re_blank_lines = re.compile('\n[ \t]*\n')

		self.name = kw.get('name', '')
		self.url = kw.get('url', '')
		self.template_file = kw.get('template_file', '')

		self.class_name = ''

	#----------------------------------------------------------------------
	def __repr__(self):
		result = ''
		for tag, args in self.tags.items():
			result += '%s\n' % tag
			arguments = args[0]
			arg_types = args[1]
			args_len = len(arguments)
			for x in xrange(0, args_len):
				result += '\t%s: %s\n' % (arguments[x], arg_types[x])
		return result

	#----------------------------------------------------------------------
	def process_file(self, filename):
		# To be implemented by subclasses
		pass

	#----------------------------------------------------------------------
	def _reformat_xml(self, input):
		"""
		Pretty-print the passed XML string

		@param input (str)
		@return result (str)
		"""
		try:
			xml = parseString(input)
			result = xml.toprettyxml('    ')
		except ExpatError:
			return input

		result = self.re_blank_lines.sub('', result)
		return result

	#----------------------------------------------------------------------
	def write_wsdl(self, filename):
		"""
		Generate and write the WSDL based on the information parsed from the
		source files.

		@param filename (str)
		"""
		messages = ''
		ports = ''
		operations = ''

		if self.name:
			name = self.name
		else:
			name = self.class_name

		for tag, args in self.tags.items():
			parts = ''
			result = ''
			arguments = args[0]
			arg_types = args[1]
			args_len = len(arguments)
			for x in xrange(0, args_len):
				arg = arguments[x]
				arg_type = arg_types[x]
				if arg == 'return':
					continue
				try:
					arg_type = type_map[arg_type]
				except KeyError:
					arg_type = '***unknown***'
				parts += '<part name="%s" type="xsd:%s"/>' % (arg, arg_type)

			if parts:
				messages += '''<message name="%s">%s</message>''' % (tag, parts)
			else:
				messages += '''<message name="%s"/>''' % (tag)

			try:
				arg_type = type_map[arg_types[arguments.index('return')]]
			except (ValueError, KeyError):
				arg_type = '***unknown***'
			messages += '''<message name="%sResponse"><part name="return" type="xsd:%s"/></message>''' % \
				(tag, arg_type)

			ports += '''<operation name="%s"><input message="tns:%s"/><output message="tns:%sResponse"/></operation>''' % \
				(tag, tag, tag)

			operations += '<operation name="%(tag)s"><soap:operation soapAction="%(url)s/%(name)sService/get_session"/>' \
						  '<input><soap:body use="literal" namespace="%(url)s/%(name)sService"/></input>' \
						  '<output><soap:body use="literal" namespace="%(url)s/%(name)sService"/></output>' \
						  '</operation>''' % { 'name': name, 'tag': tag, 'url': self.url }

		fp = open(self.template_file, 'r')
		template = fp.read()
		fp.close()

		s = Template(template).substitute(
				name=name,
				url=self.url,
				ports=ports,
				operations=operations,
				messages=messages)
		s = self._reformat_xml(s)

		target = open(filename, 'w')
		target.write(s)
		target.close()


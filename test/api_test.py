#!/usr/bin/env python
#
# just a test file


class MyFancySoapService:
	"""
	A very fancy and completely useless test class
	"""

	def __init__(self):
		pass


	def noop(self):
		"""
		Does nothing.
		@return (str) session_key
		"""

		return ''

	def get_session(self, username, password):
		"""
		Creates a session

		@param username (str)
		@param password (str)
		@return (str) session_key
		"""

		return ''

	def get_a_int(self, session_key, name, really):
		"""
		Get a value

		@param session_key (str)
		@param name (str)
		@param really (bool)
		@return value (int)
		"""

		return 0

	def get_a_time(self, session_key, name, invalid):
		"""
		Get a value

		@param session_key (str)
		@param name (str)
		@param invalid (tuple)
		@return value (datetime)
		"""

		return 0
